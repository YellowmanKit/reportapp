import { createStore, combineReducers, applyMiddleware } from 'redux'
import thunk from 'redux-thunk'

import { main } from './control/main/reducer'
import { ui } from './control/ui/reducer'
import { page } from './control/page/reducer'
import { photo } from './control/photo/reducer'
import { map } from './control/map/reducer'

import { report } from './data/report/reducer'

const reducer = combineReducers({
  main, ui, page, photo, map,
  report
})

export default createStore(reducer, {}, applyMiddleware(thunk))
