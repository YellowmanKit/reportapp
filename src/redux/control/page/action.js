export const push = view => { return { type: 'push', payload: view } }
export const pull = () => { return { type: 'pull' } }
export const setSubView = (subView, init) => { return { type: 'setSubView', payload: { subView, init } } }
export const walkSubView = step => { return { type: 'walkSubView', payload: step } }
export const backToIndex = () => { return { type: 'backToIndex' } }
