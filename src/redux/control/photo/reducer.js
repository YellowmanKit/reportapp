import { reducer } from '../../redux'

export const photo = (
  state = initState, action) => reducer(state, action, result)

const initState = {
  selected: 0,
  photos: [null,null,null,null,null,null,null,null]
}

const result = (state, { type, payload }) => {
  switch (type) {
    case 'resetPhoto':
      return { ...initState, photos: [null,null,null,null,null,null,null,null] }
    case 'add':
      var { photos } = state
      for(var i=0;i<photos.length;i++){
        if(!photos[i]){
          photos[i] = payload
          break
        }
      }
      return { ...state, photos }
    case 'remove':
      var { photos } = state
      photos[payload] = null
      return { ...state, photos }
    default:
      return state
  }
}
