import { reducer } from '../../redux'

export const map = (
  state = {
    init: false,
    region: {
      latitude: 22.25,
      longitude: 114.18,
      latitudeDelta: 0.08,
      longitudeDelta: 0.03
    },
    markers: []
  }, action) => reducer(state, action, result)

const result = (state, { type, payload }) => {
  switch (type) {
    default:
      return state
  }
}
