import axios from 'axios'
import { to, action } from '../../redux'
import { API, KEY } from '@env'
import { address } from '../../../utility/Address'

var err, res
export const onRemove = ({ markers }, count ) => {
  return async dispatch => {
    markers[count - 1] = false
    dispatch(action.set('location' + count, ''))
    dispatch(action.set('markers', markers))
    dispatch(action.set('address', ''))
    dispatch(action.set('latitude', ''))
    dispatch(action.set('longitude', ''))
  }
}

export const onPress = ({ markers, region }, coordinate) => {
  return async dispatch => {
    dispatch(action.set('loading', true))
    markers = [{ count: 1, coordinate }]
    var { latitude, longitude } = coordinate;
    [err, res] = await address.find(coordinate)
    dispatch(action.set('address', res))
    dispatch(action.set('latitude', latitude))
    dispatch(action.set('longitude', longitude))
    dispatch(action.set('markers', markers))
    dispatch(action.set('region', { ...region, ...coordinate }))
    dispatch(action.set('loading', false))
  }
}
