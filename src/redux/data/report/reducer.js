import { reducer } from '../../redux'

export const report = (
  state = initState,
  action) => reducer(state, action, result)

const initState = {
  mode: '',
  date: '',
  time: '',
  datetimeString: Date.now(),
  address: '',
  latitude: '',
  longitude: '',
  isRoadSign: false,
  isAccident: false,
  description: ''
}

const result = (state, { type, payload }) => {
  switch (type) {
    case 'resetReport':
      return initState
    default:
      return state
  }
}
