import axios from 'axios'
import { to, action, IOS } from '../../redux'
import { API, KEY } from '@env'
import { address } from '../../../utility/Address'

var err, res
export const upload = async files => {
  var files = files.filter(file => file !== null)
  if(files.length === 0){ return [] }
  var err, res
  var forms = new FormData()
  files.map(({ name, type, uri }) => {
    const s_type = uri.split('.')[uri.split('.').length - 1]
    const s_name = uri.split('/')[uri.split('/').length - 1]
    const data = {
      name: name? name: s_name,
      type: type? type: 'image/' + s_type,
      uri
    }
    forms.append('files', data)
  });
  [err, res] = await to(fetch(API + 'upload', { method: 'POST', body: forms }))
  if(err){ return [] }
  const response = await res.json()
  return response.filenames
}

export const locate = coordinate => {
  return async dispatch => {
    [err, res] = await address.find(coordinate)
    dispatch(action.set('address', res))
  }
}

export const submit = ({ report, photo }) => {
  return async dispatch => {
    if(report.mode === 'realTime'){
      const current = new Date().toJSON()
      const split = current.split('T')
      report.date = split[0]
      report.time = split[1]
    }
    dispatch(action.set('loading', true));
    const filenames = await upload(photo.photos)
    report['photos'] = '[' + filenames.join() + ']';
    [err, res] = await to(axios.post(API + 'submit', report))
    dispatch(action.set('view','done'))
    dispatch(action.set('markers',[]))
    dispatch({ type: 'resetPhoto' })
    dispatch({ type: 'resetReport' })
    dispatch(action.set('loading', false))
  }
}
