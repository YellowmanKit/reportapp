import React from 'react'
import Component from 'reportapp/src/component/Component'
import { View as Div, Dimensions } from 'react-native'
import Page from './page/Page'
import Camera from './mask/Camera'
import Loading from './mask/Loading'

export default class Main extends Component {

  init({ action }){
    action.main.set('status', 'ready')
    action.ui.set('window', Dimensions.get('window') )
  }

  content = ({ store: { ui: { window, style: { flex } } } }) =>
    <Div style={{ ...window, ...flex.ColCC }}>
      {this.main(this.app)}
      <Camera app={this.app}/>
      <Loading app={this.app}/>
    </Div>

  main = ({ store: { main: { status } } }) => {
    switch (status) {
      case 'ready':
        return <Page app={this.app}/>
      default:
        return null
    }
  }

}
