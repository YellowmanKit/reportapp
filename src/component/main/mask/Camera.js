import React from 'react'
import { View as Div } from 'react-native'
import Component from 'reportapp/src/component/Component'
import { RNCamera } from 'react-native-camera';

export default class Camera extends Component {

content = ({ store: { main: { camera } } }) => {
  if(!camera){ return null }
  return this.camera(this.app)
}

camera = ({ func, button, store: { ui: { window, style: { flex } } }, action: { main } }) =>
  <Div style={{ ...window, ...flex.ColCC, position: 'absolute', backgroundColor: 'black' }}>
    <RNCamera
      ref={ref => { this.cam = ref }}
      style={{ ...this.sizePercent(1,1), position: 'absolute' }}
      type={RNCamera.Constants.Type.back}
      flashMode={RNCamera.Constants.FlashMode.off}
      androidCameraPermissionOptions={{
        title: 'Permission required',
        message: 'Allow mLang to access your phone camera?',
        buttonPositive: 'Yes',
        buttonNegative: 'No',
      }}
      captureAudio={false}
    />
    <Div style={{ position: 'absolute', top: 5, left: 5 }}>
      {button.top('<', ()=>{ main.set('camera', false) })}
    </Div>
    <Div style={{ ...this.size(1,0.8), ...flex.ColCE}}>
      {button.shot('Shot', ()=>{ this.onCameraShot(this.app) })}
    </Div>
  </Div>

  onCameraShot = async ({ func, action }) => {
    var err, data
    if (this.cam) {
      [err, data] = await func.to(this.cam.takePictureAsync({ quality: 0.5, base64: true }))
      if(data){ action.photo.add(data) }
    }
    action.main.set('camera', false)
  }

}
