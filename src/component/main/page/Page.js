import React from 'react'
import { View as Div, BackHandler } from 'react-native'
import Component from 'reportapp/src/component/Component'
import Index from './view/index/Index'
import Report from './view/report/Report'
import Done from './view/done/Done'

export default class Page extends Component {

  content = ({ store: { ui: { window, style: { flex } } } }) =>
    <Div style={{ ...window, ...flex.ColCC }}>
      {this.page(this.app)}
    </Div>

  page = ({ store: { page: { view } } }) => {
    switch (view) {
      case 'index':
        return <Index app={this.app}/>
      case 'report':
        return <Report app={this.app}/>
      case 'done':
        return <Done app={this.app}/>
      default:
        return null
    }
  }

}
