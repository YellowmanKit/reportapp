import React from 'react'
import { View as Div, BackHandler } from 'react-native'
import Component from 'reportapp/src/component/Component'

export default class Index extends Component {

  content = ({ store: { ui: { window, style: { flex } } }, action, text, button, element }) =>
    <Div style={{ ...window, ...flex.ColCC }}>
      {element.logo(require('reportapp/res/image/hku.png'))}
      {this.verGap(0.02)}
      {text.logo('Crowed-Sourced Traffic Sign and Accident Report')}
      {this.verGap(0.2)}
      {button.entry('Real Time Report', 'green', ()=>{
        action.page.push('report')
        action.report.set('mode','realTime')
      })}
      {this.verGap(0.02)}
      {button.entry('After Event Report', 'blue', ()=>{
        action.page.push('report')
        action.report.set('mode','afterEvent')
      })}
      {this.verGap(0.02)}
      {button.entry('Quit System', 'red', ()=>BackHandler.exitApp())}
    </Div>

}
