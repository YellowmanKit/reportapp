import React from 'react'
import { View as Div, BackHandler } from 'react-native'
import Component from 'reportapp/src/component/Component'

export default class Done extends Component {

  content = ({ store: { ui: { window, style: { flex } } }, action, text, button, element }) =>
    <Div style={{ ...window, ...flex.ColCC }}>
      {text.title('Thanks for Your submission!')}
      {this.verGap(0.04)}
      {button.standard('Back to index',()=>action.page.backToIndex())}
    </Div>

}
