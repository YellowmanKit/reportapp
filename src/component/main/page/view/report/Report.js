import React from 'react'
import { View as Div } from 'react-native'
import Component from 'reportapp/src/component/Component'
import Top from 'reportapp/src/component/module/Top'
import Nav from 'reportapp/src/component/module/Nav'
import Photo from './sub/Photo'
import Location from './sub/Location'
import Detail from './sub/Detail'
import Confirm from './sub/Confirm'
import Datetime from './sub/Datetime'

export default class Report extends Component {

  init({ action, store: { report: { mode } } }){
    action.page.setSubView(mode === 'afterEvent'? 'datetime':'photo')
    var subs = ['photo','location','detail','confirm']
    if(mode === 'afterEvent'){
      subs.splice(0,0,'datetime')
    }
    this.state = { subs }
  }

  content = ({ store: { report: { mode }, ui: { window, style: { flex } } }, action, button, text }) =>
    <Div style={{ ...window, ...flex.ColCS, backgroundColor: 'white' }}>
      {this.verGap(0.02)}
      <Top app={this.app} title={mode === 'realTime'?'Real Time Report':'After Event Report'}/>
      <Nav app={this.app} subs={this.state.subs}/>
      {this.sub(this.app)}
    </Div>

  sub = ({ store: { page: { subView } } }) => {
    switch (subView) {
      case 'datetime':
        return <Datetime app={this.app}/>
      case 'photo':
        return <Photo app={this.app}/>
      case 'location':
        return <Location app={this.app}/>
      case 'detail':
        return <Detail app={this.app}/>
      case 'confirm':
        return <Confirm app={this.app}/>
      default:
        return null
    }
  }

}
