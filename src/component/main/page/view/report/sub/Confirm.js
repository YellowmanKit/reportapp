import React from 'react'
import { View as Div, Platform } from 'react-native'
import Component from 'reportapp/src/component/Component'
import Cell from 'reportapp/src/component/module/Cell'

export default class Confirm extends Component {

  content = ({ store: { photo, report: { isRoadSign, isAccident, description,
  address, latitude, longitude, date, time },
    ui: { window, style: { flex } } }, action, button, text, input, func }) =>
    <Div style={{ ...this.size(1,0.85,true), ...flex.ColCS }}>
      {text.title('Confirm')}
      {this.verGap(0.01)}
      {text.standard('Please confirm your report')}
      {this.verGap(0.01)}
      <Div style={{ ...this.size(0.9,0.315,true), ...flex.RowSS,
         padding: '1%', flexWrap: 'wrap' }}>
        {photo.photos.map((photo, index)=>
          <Cell key={index} index={index} photo={photo} app={this.app} disabled={true}/>)}
      </Div>
      {this.verGap(0.02)}
      {text.small(address)}
      {this.verGap(0.01)}
      {text.small(latitude)}
      {this.verGap(0.01)}
      {text.small(longitude)}
      {this.verGap(0.01)}
      {text.small(isRoadSign? 'Road Sign': isAccident?'Accident':'')}
      {this.verGap(0.01)}
      {text.small(description)}
      {this.verGap(0.01)}
      {text.small(date)}
      {this.verGap(0.01)}
      {(date !== '') && text.small(func.timeString({ date, time }))}
      {this.verGap(0.02)}
      {button.standard('Submit', ()=>action.report.submit(this.app.store))}
    </Div>

}
