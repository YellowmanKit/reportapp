import React from 'react'
import { View as Div, Platform } from 'react-native'
import Component from 'reportapp/src/component/Component'
import Cell from 'reportapp/src/component/module/Cell'
import DocumentPicker from 'react-native-document-picker'
import { launchImageLibrary } from 'react-native-image-picker';

export default class Photo extends Component {

  content = ({ store: { photo, ui: { window, style: { flex } } },
    action, button, text, element }) =>
    <Div style={{ ...this.size(1,0.85,true), ...flex.ColCS }}>
      {text.title('Photo')}
      {this.verGap(0.01)}
      {text.standard('Please take/select photo showing the problem.')}
      <Div style={{ ...this.size(0.9,0.1,true), ...flex.RowCC }}>
        {button.standard('Camera', ()=>action.main.set('camera',true))}
        {this.horGap(0.05)}
        {button.standard('Storage', ()=>this.onFileSelected(this.app))}
        {this.horGap(0.05)}
        {button.standard('Gallery', ()=>launchImageLibrary({ mediaType: 'photo'}, result => {
          if(result.assets){
            const data = result.assets[0]
            action.photo.add(data)
          }
        }))}
      </Div>
      <Div style={{ ...this.size(0.9,0.315,true), ...flex.RowSS,
         padding: '1%', flexWrap: 'wrap' }}>
        {photo.photos.map((photo, index)=>
          <Cell key={index} index={index} photo={photo} app={this.app}/>)}
      </Div>
      {photo.photos[photo.selected] &&
        element.photo(photo.photos[photo.selected])}
      {this.verGap(0.02)}
      {photo.photos[photo.selected] &&
        button.standard('Remove', ()=>action.photo.remove(photo.selected))}
    </Div>

  onFileSelected = async ({ func: { to }, action }) => {
    var err, data;
    [err, data] = await to(DocumentPicker.pick({
      type: Platform.OS === 'ios'? 'public.image': 'image/*' }))
    if(data){ action.photo.add(data) }
  }

}
