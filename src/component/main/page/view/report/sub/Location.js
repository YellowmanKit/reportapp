import React from 'react'
import { View as Div, Platform } from 'react-native'
import Component from 'reportapp/src/component/Component'
import Map from 'reportapp/src/component/module/Map'

export default class Location extends Component {

  content = ({ store: { report, ui: { window, style: { flex } } },
    action, text }) =>
    <Div style={{ ...this.size(1,0.85,true), ...flex.ColCS }}>
      {text.title('Location')}
      {this.verGap(0.01)}
      {text.standard('Please mark the location on the map')}
      {this.verGap(0.01)}
      {<Map app={this.app}/>}
      {this.verGap(0.02)}
      {text.small(report.address)}
      {this.verGap(0.01)}
      {text.small(report.latitude)}
      {this.verGap(0.01)}
      {text.small(report.longitude)}
    </Div>

}
