import React from 'react'
import { View as Div, Platform } from 'react-native'
import Component from 'reportapp/src/component/Component'

export default class Detail extends Component {

  content = ({ store: { report: { isRoadSign, isAccident, description },
    ui: { window, style: { flex } } }, action, button, text, input }) =>
    <Div style={{ ...this.size(1,0.85,true), ...flex.ColCS }}>
      {text.title('Detail')}
      {this.verGap(0.01)}
      {text.standard('Please provide any detail about your report')}
      {this.verGap(0.02)}
      {input.choice('Road Sign',
        ()=>action.report.set('isRoadSign', !isRoadSign), isRoadSign)}
      {this.verGap(0.01)}
      {input.choice('Accident',
        ()=>action.report.set('isAccident', !isAccident), isAccident)}
      {this.verGap(0.01)}
      {text.small('Description:')}
      {input.textAnswer(description, value=>action.report.set('description', value), true)}
    </Div>

}
