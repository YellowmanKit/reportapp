import React from 'react'
import { View as Div, Platform } from 'react-native'
import Component from 'reportapp/src/component/Component'
import DateTimePicker from '@react-native-community/datetimepicker'

export default class Datetime extends Component {

  content = ({ store: { report, main: { datetime }, ui: { window, style: { flex } } },
    action, button, text, input, func }) =>
    <Div style={{ ...this.size(1,0.85,true), ...flex.ColCS }}>
      {text.title('Date & Time')}
      {this.verGap(0.01)}
      {text.standard('Please provide date and time of the event')}
      {this.verGap(0.02)}
      {text.standard(report.date)}
      {this.verGap(0.02)}
      {(report.date !== '') &&
        text.standard(func.timeString(report))}
      {this.verGap(0.02)}
      {Platform.OS !== 'ios' && button.standard('Select date & time', ()=>action.main.set('datetime', 'date'))}
      {this.verGap(0.02)}
      {Platform.OS !== 'ios' && datetime && <DateTimePicker value={new Date()} mode={datetime} onChange={(event, selected)=>{
        action.main.set('datetime', datetime==='date'?'time':false)
        action.report.set(datetime, selected.toJSON().split('T')[datetime==='date'?0:1])
      }}/>}
      {Platform.OS === 'ios' &&
      <Div style={{ ...this.size(0.75,0.5,true) }}>
        <DateTimePicker
        testID="dateTimePicker"
        display="default"
        value={new Date(report.datetimeString)} mode={'date'} onChange={(event, selected)=>{
          action.report.set('datetimeString', selected.toString())
          action.report.set('date', selected.toJSON().split('T')[0])
        }}/>
        {this.verGap(0.01)}
        <DateTimePicker
        testID="dateTimePicker"
        display="default"
        value={new Date(report.datetimeString)} mode={'time'} onChange={(event, selected)=>{
          action.report.set('datetimeString', selected.toString())
          action.report.set('time', selected.toJSON().split('T')[1])
        }}/>
      </Div>}
    </Div>

}
