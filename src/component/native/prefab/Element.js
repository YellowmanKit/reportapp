import React from 'react'
import Native from '../Native'

export default class Element extends Native{

  photo = source => this.image(source, { ...this.size(0.3,0.2),
    backgroundColor: this.ui.color.lightGrey, resizeMode: 'contain' })

  logo = source => this.image(source, this.size(0.2,0.2))

}
