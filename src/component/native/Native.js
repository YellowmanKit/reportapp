import React from 'react'
import Component from 'reportapp/src/component/Component'
import { View as Div, Text, Image, TouchableOpacity } from 'react-native'
import { TextInput } from 'react-native-normalized'
import ParsedText from 'react-native-parsed-text';

export default class Native extends Component {

  image = (source, style) =>
    <Image style={style} source={source}/>

  text = ({ text, key }, style) =>
    <Text style={style} key={key}>{text}</Text>

  parsedText = ({ text, bolds, key }, style) =>
    <ParsedText style={style} key={key} parse={bolds.map(bold => {
      return { pattern: bold, style: { ...style, fontWeight: 'bold' } } })}>
      {text}</ParsedText>

  parsed = ({ text, bolds, key }, style) =>
      <ParsedText
        style={style}
        parse={bolds? bolds.map(item => { return {
          pattern: new RegExp(item),
          style: { fontWeight: 'bold' } } }):[]}>
        {text}
      </ParsedText>

  background = url =>
    <Image style={{ position: 'absolute', width: '100%', height: '100%', resizeMode: 'stretch'  }} source={url?url:null} />

  checkbox = ({ value, onPress }, style) =>
    <TouchableOpacity style={{...{ backgroundColor: 'white', borderWidth: 1, borderColor: 'black', borderRadius: 3 }, ...style}} onPress={onPress}>
      {value && this.background(require('reportapp/res/image/tick.png')) }
    </TouchableOpacity>

  checkboxBar = (option, style) =>
    <Div style={style.view}>
      {this.checkbox(option, style.checkbox)}
      {this.horGap(0.02)}
      {this.text(option, style.text)}
    </Div>

  textArea = ({ value, onChangeText, multiline }, style) =>
      <TextInput
        value={value? value: ''}
        onChangeText={onChangeText}
        style={style}
        autoCapitalize={'none'}
        multiline={multiline}
        textAlignVertical={'top'}
      />

  touchableOpacity = ({ text, onPress }, style) =>
    <TouchableOpacity style={{ ...this.ui.style.flexColCC, ...style.view }} onPress={onPress}>
      {text && this.text({ text }, style.text)}
    </TouchableOpacity>

}
