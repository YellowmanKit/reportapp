import React from 'react'
import { View as Div, TouchableOpacity, Image } from 'react-native'
import Component from 'reportapp/src/component/Component'

export default class Cell extends Component {

  content = ({ store: { photo: { selected }, ui: { window, color, style: { flex } } },
    action }, { index, photo, disabled }) =>
    <TouchableOpacity style={{ ...this.size(0.2,0.125, true), ...flex.RowCC,
      backgroundColor: color.lightGrey, justifyContent: 'space-evenly',
      margin: '1%', borderWidth: (!disabled && selected === index)? 2:0, borderColor: 'grey' }}
      onPress={()=>{ action.photo.set('selected', index) }}>
      {photo &&
        <Image source={photo} style={{ ...this.sizePercent(0.975,0.975), resizeMode: 'contain' }}/>}
    </TouchableOpacity>

}
