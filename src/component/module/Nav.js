import React from 'react'
import { View as Div, TouchableOpacity } from 'react-native'
import Component from 'reportapp/src/component/Component'
import GetLocation from 'react-native-get-location'

export default class Nav extends Component {

  init({ action },{ subs }){
    action.page.set('subs', subs)
  }

  content = ({ store: { page, ui: { window, color, style: { flex } } },
    action, button, text }, { subs }) =>
    <Div style={{ ...this.size(0.75,0.05, true), ...flex.RowCC,
      justifyContent: 'space-evenly', alignItems: 'center' }}>
      {button.nav('<', ()=>action.page.walkSubView(-1))}
      {subs.map((sub, index) =>
        <TouchableOpacity onPress={()=>action.page.setSubView(subs[index])} key={sub} style={{ ...this.size(0.0175,0.0175),
          backgroundColor: page.subView === sub?'grey':color.lightGrey, borderRadius: 100 }}/>)}
      {button.nav('>', ()=>action.page.walkSubView(1))}
    </Div>

}
