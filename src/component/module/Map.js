import React from 'react'
import { View as Div } from 'react-native'
import Component from 'reportapp/src/component/Component'
import MapView, { Marker, PROVIDER_GOOGLE } from 'react-native-maps';
import GetLocation from 'react-native-get-location'

export default class Map extends Component {

  init({ store: { map }, action }){
    if(map.init){ return }
    GetLocation.getCurrentPosition({
      enableHighAccuracy: true,
      timeout: 15000
    }).then(location => {
      action.map.set('region', { ...map.region, ...location })
      action.map.set('init', true)
      action.map.onPress(map, location)
    })
  }

  content = ({ store: { map, ui: { style: { flex } } }, action }) =>
      map.init?<MapView
        provider={PROVIDER_GOOGLE}
        style={{ ...this.size(0.9, 0.4, true) }}
        onPress={data=>action.map.onPress(map, data['nativeEvent'].coordinate)}
        initialRegion={map.region}>
        {map.markers.map(({ count, coordinate }) => count?(
          <Marker
            key={count}
            coordinate={coordinate}
            title={'' + count}
            description={''}
            onPress={()=>action.map.onRemove(map, count)}
          />
        ):null)}
      </MapView>:null

}
