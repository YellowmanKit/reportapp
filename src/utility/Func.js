export default class Func {

  timeString = ({ date, time }) => new Date(date + 'T' + time).toTimeString()

  to = (promise) => promise.then(data => [null, data]).catch(err => [err])

}
